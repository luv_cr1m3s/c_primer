//  names.c -- prints first and last name with length of each word on the next
//  line
#include <stdio.h>
#include <string.h>

int main(void){

	char first[15];
	char last[15];
	int len_first;
	int len_last;

	printf("Please enter your first and last name: ");
	scanf("%s %s", first, last);
	len_first = strlen(first);
	len_last = strlen(last);
	
	printf("%s %s\n", first, last);
	printf("%*d %*d\n", len_first, len_first, len_last, len_last);
	printf("%-*d %-*d\n", len_first, len_first, len_last, len_last);

	return 0;
}
