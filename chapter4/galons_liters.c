/* galons_liters.c -- converts galons to liters and calculates fuel consumption
 * in american and european style */
#include <stdio.h>

#define GALON 3.785 // 1 galon -> 3.785 liters
#define MILE 1.609  // i mile -> 1.609 km

int main(void){

	double miles, galons, mile_per_galon, km, liters, liters_100km;

	printf("Enter number of miles traveled and amount of fuel used in galons:\n");
	printf("miles-->");
	scanf("%lf", &miles);
	printf("galons of fuel-->");
	scanf("%lf", &galons);

	mile_per_galon = miles / galons;
	
	km = MILE * miles;
	liters = GALON * galons;
	liters_100km = 100 * liters / km;

	printf("American style:\n");
	printf("Miles traveled: %.3lf, galons used: %.3lf, mile-per-galon: %.3lf.\n",
			miles, galons, mile_per_galon);
	
	printf("European style:\n");
	printf("km traveled: %.3lf, liters used: %.3lf, liters-per-100 km: %.3lf.\n", 
			km, liters, liters_100km);
	return 0;
}
