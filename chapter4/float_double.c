// float_double.c -- compartes result of printf for float and double
#include <stdio.h>
#include <float.h>

int main(void){

	double dub = 1.0/3.0;
	float flo = 1.0/3.0;

	printf("Double 1.0/3.0: %.4lf %.12lf %.16lf %.24lf\n", dub, dub, dub, dub);
	printf("Float 1.0/3.0: %.4f %.12f %.16f\n", flo, flo, flo);

	printf("FLT_DIG: %d, DBL_DIG: %d\n", FLT_DIG, DBL_DIG);
	return 0;
}
