/* meters.c -- asks height in cm and converts it into meters */
#include <stdio.h>

int main(void){

	float cm;
	char name[10];

	printf("Please enter your name and your height in cms: ");
	scanf("%s %f", name, &cm);

	printf("%s, you are %f meters tall.\n", name, cm / 100);

	return 0;
}
