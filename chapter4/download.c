// download.c -- calculates time for download based on user input
#include <stdio.h>

int main(void){

	float size, speed;

	printf("Enter download speed in Mbs and file size in MB: ");
	scanf("%f %f", &speed, &size);

	printf("At %.2f megabits per second, a file of %.2f megabytes"
			"downloads in %.2f seconds.\n", speed, size, (size * 8) / speed);

	return 0;
}
