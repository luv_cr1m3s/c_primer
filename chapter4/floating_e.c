/* floating_e.c -- asks about two numbers and prints them with different
 * formatting */
#include <stdio.h>

int main(void){

	float one, two;

	printf("Please enter two numbers, first one with 'e', and second one with 'E':");
	
	scanf("%f %F", &one, &two);

	printf("%f %f\n", one, two);

	return 0;
}

