/* last_firts.c -- asks about first and the last name, and prints them in
 * reversed order*/
#include <stdio.h>

int main(void){

	char first[10];
	char last[10];

	printf("Enter your first and last name: ");
	
	scanf("%s %s", first, last);
	printf("Here we have your last name: %s, and first name: %s\n",
			last, first);

	return 0;
}

