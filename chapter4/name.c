/* name.c --  asks about name and print it with different formatting */
#include <stdio.h>
#include <string.h>

int main(void){

	char name[10];
	int length;
	printf("Enter your name: ");
	scanf("%s", name);
	
	length  = strlen(name) + 3;

	// a. prints name enclosed in double quotation marks
	printf("\"%s\"\n", name);

	// b. prints in a field 2 chars wide, with the whole field in quotes and name
	// on the right end of the field
	printf("\"%20s\"\n", name);

	// c. same as before but name on the left side of the field
	printf("\"%-20s\"\n", name);

	// d. prints in the field 3 chars wider than the name
	printf("\"%*s\"\n", length, name);

	return 0;
}

