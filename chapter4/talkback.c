/* talkback.c -- program that calculates your body volume */
#include <stdio.h>
#include <string.h>

#define DENSITY 1.1

int main(void){

	float weight, volume;
	int size, letters;
	char name[40];

	printf("Enter your first name: ");
	scanf("%s", name);
	printf("\nEnter your weight in kg: ");
	scanf("%f", &weight);
	size = sizeof(name);
	letters = strlen(name);
	volume = weight / DENSITY;
	printf("\nWell %s, your volume is %2.2f liters.\n",  name, volume);
	printf("Also, your first name name hs %d letters.\n", letters);
	printf("And it takes %d bytes to store your name.\n",  size);

	return 0;
}


