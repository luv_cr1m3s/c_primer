### Chapter 4 Character String and Formatted Input/Output
---

## Strings

* `'\0'` -- null character.
* `scanf()` can't handle white spaces and similar symbols.

```
'x' --> x
"x" --> x \0
```

* `C99` and `C11` standards use a `%zd` specifier for the type used by `sizeof` and `strlen()`.

## Constants and the C Preprocessor

* Example of the `manifest constant`. When program is compiled, the value 0.015 will be substituted everywhere you have used `TAXRATE`. This is called a `compile-time substitution`.

```c
#define TAXRATE 0.0.15
```
* `C90` added keyword `const` for more flexible way of constant declaration (lets you declare a type and allows better control over which parts of a program can use  the constant).

```c
const int MONTHS = 12;
```

* `printf()` returns number of printed chars with unseen one (newline, tabs...).

* If you follow one quoted string constant with another, separated only with whitespace, C treats the combination as a single string. This could be used in `printf()'` to separate formatting string.

* To read a value for one of the basic variable types precede variable name with `&` in `scanf()`, if you want to read a string don't use `&`.

* You can use multiply multiply conversion specification if they are separated with '\n', ' ' or '\t' with exception for chars.

* In `scanf()` for `%s` input begins with the first non-whitespace character and include everything up to the next whitespace character.

* `*` used to provide formatting information for `printf()` function:

```c
printf("Weight = %*.*f\n", width, precision, weight);
```

* `*` in `scanf()` used to skip over corresponding input:

```c
anf("%*d %*d %d", value_that_will_be_stored);
```





