/* test.c -- program to test wrong constant definition */
#include <stdio.h>

// gcc won't compile it
// 'error: expect expression befoire '='
#define TOES = 20

int main(void){
	
	int digit = 10;
	int fingers;

	fingers = digit + TOES;

	printf("Here is your fingers: %d", fingers);

	return 0;
}
