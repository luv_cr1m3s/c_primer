/* test_print -- program to test print formatting for strings*/
#include <stdio.h>
#include <string.h>
#include <unistd.h>

int main(void){
	
	char hello[] = "Hello world!   ";
	int text_start = 0;
	int text_end = 11;
	int str_len = strlen(hello);
	int count;
	int i = 0;
	while(1){
		count = text_start;
		printf("[");
		for( i = 0; i <= text_end; i++){
			if(count >= str_len)
				count = 0;
			printf("%c", hello[count]);
			count += 1;
		}
		printf("]");
		if (text_start >= str_len)
			text_start = 0;
		else
			text_start += 1;
		// it won't work without fflush function 
		// since I don't know how to full buffer or return pointer into the very
		// beginning after '\n'
		fflush(stdout);
		// used usleep() for microseconds
		// just sleep() not so good
		usleep(100000);
		printf("\r");
	}
/*
	char hello[] = "Hello World!";
	int length = strlen(hello);
	int count = length;

	while(1){
	
		printf("[%*.*s]", length, count, hello);
		fflush(stdout);
		usleep(100000);
		printf("\r");
		if(count <= 0)
			count = length;
		else
			count--;
	}
*/
	return 0;
}
