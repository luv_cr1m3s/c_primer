### Chapter 2 Introducing C

* 5 types of statements in C:
	* declaration
	* assignment
	* function
	* control
	* null

* `Preprocessing` -- preparatory work on source code before compiling.
* `Multibyte` characters could be used in strings but not in declaration.

