#include <stdio.h>

int main(void){

	float inch_val;
	printf("Please enter value in inches: ");
	scanf("%f", &inch_val);

	printf("%.2f inches converted to centimeters: %.2f cm\n", inch_val, inch_val * 2.54);

	return 0;
}
