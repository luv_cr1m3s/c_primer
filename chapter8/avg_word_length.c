/* avg_word_length.c -- reads user input and counts avg number of letters in
 * user input */
#include <stdio.h>
#include <ctype.h>

int main(void){

	// words -- number of words
	// length -- length of all words
	// flag indicates that we are still in the word
	int words, length, flag;
	char ch;

	words = length = 0;

	printf("Please put your input below:\n");
	printf("--> ");

	while((ch = getchar()) != EOF){
		
		if( ispunct(ch) || isspace(ch)){
			if (flag == 1){
				words++;
				flag = 0;
			}
		}
		else{
			flag = 1;
			length++;
		}
	}	

	printf("I found: %d words with sum length: %d\n", words, length);
	printf("Average length of the words in your input:" 
			" %.2f\n", (float)length / (float)words);

	return 0;
}
