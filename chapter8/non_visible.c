/* non_vivisble.c -- prints decimal values of user input + special symbols for
 * whitespace etc. */
#include <stdio.h>

int main(void){

	char ch;
	
	printf("Put your input below\n");
	printf("--> ");

	while((ch = getchar()) != EOF){
		switch(ch){
			case ' ': printf("\\s");
								break;
			case '\t': printf("\\t");
								break;
			case '\n': printf("\\n\n");
								break;
			default:	printf("%d", ch);
								break;
		}
	}

	printf("\n");

	return 0;
}
