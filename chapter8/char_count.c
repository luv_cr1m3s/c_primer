/* char_count.c -- reads user input and counts chars till EOF */
#include <stdio.h>

int main(void){

	int char_count = 0;
	
	printf("Place your input below:\n");
	printf("--> ");

	while( getchar() != EOF)
		char_count++;

	printf("Here is number of entered chars: %d\n", char_count);

	return 0;
}

