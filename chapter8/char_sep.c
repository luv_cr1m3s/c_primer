/* char_sep.c -- reads user input and counts lower/uppercase letter and other */
#include <stdio.h>
#include <ctype.h>

int main(void){

	int upper, lower, other, all;
	char ch;

	upper = lower = other = all = 0;

	printf("Put your input below:\n");
	printf("--> ");

	while((ch = getchar()) != EOF){
		
		if(islower(ch)) { 
			lower++;
		}
		else if(isupper(ch)) {
			upper++;
		}
		else {
			other++;
		}
		all++;		
	}

	printf("Inn your input present:\n");
	printf("Upper case letters: %d\n", upper);
	printf("Lower case letters: %d\n", lower);
	printf("Other chars: %d\n", other);
	printf("Number of all chars: %d\n", all);
	return 0;
}
