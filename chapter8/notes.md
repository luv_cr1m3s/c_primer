### Chapter 8 Character Input/Output and Validation
---

* Buffered 
 * fully buffered I/O -- flushed when buffer is full
 * line-buffered I/O -- flushed when newline char occurs

* On `UNIX` 'ioctl()' function provides control over I/O. 

* Unbuffered input.

```c
while(getchar() != '\n')
	continue // -- wa to omit newline character in user input
```


