/* s_calc.c -- simple calculator*/
#include <stdio.h>

char get_choice(void);
char get_first(void);
float get_float(void);

int main(void){

	float num1, num2, result;
	char choice;

	while((choice = get_choice()) != 'q'){
		printf("Please enter first number: ");
		num1 = get_float();
		printf("Please enter second number: ");
		num2 = get_float();

		while(choice == 'd' && num2 == 0){
			printf("Please enter other number than 0: ");
			num2 = get_float();
		}

		switch(choice){
			case 'a': result = num1 + num2;
								printf("%.3f + %.3f = %.3f\n", num1, num2, result);
								break;
			case 'm': result = num1 * num2;
								printf("%.3f * %.3f = %.3f\n", num1, num2, result);
								break;
			case 'd': result = num1 / num2;
								printf("%.3f / %.3f = %.3f\n", num1, num2, result);
								break;
			case 's': result = num1 - num2;
								printf("%.3f - %.3f = %.3f\n", num1, num2, result);
								break;
			default:	printf("Something gone wrong!\n");
								break;
		}
	}

	printf("Bye!\n");

	return 0;
}

char get_choice(void){
	int ch;
	printf("Enter operation  of your choice:\n");
	printf("a. addition				m. multiplication\n");
	printf("d. division				s. substraction\n");
	printf("q. quit\n");
	printf("--> ");

	ch = get_first();
	
	while(ch != 'a' && ch != 'd' && ch != 'm' && ch != 'q' && ch != 's'){
			printf("Please respond with a, m, s, d or q: \n");
			ch = get_first();
	}

	return ch;
}

char get_first(void){
	char ch;
	// space before '%c' lets skip newline
	scanf(" %c", &ch);
	
	return ch;
}

float get_float(void){
	float input;
	char ch;

	while(scanf("%f", &input) != 1){
		while((ch = getchar()) != '\n')
			putchar(ch);

		printf("Please enter valid float number: ");
	}

	return input;
}
