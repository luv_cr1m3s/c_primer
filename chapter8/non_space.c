/* non_space.c -- returns first non-whitespace charscter*/
#include <stdio.h>

char get_non_space(void);

int main(void){

	char res = get_non_space();

	printf("%c\n", res);
	return 0;
}

char get_non_space(void){
	char ch;

	while((ch = getchar()) == ' ')
			printf("It's a space, try again!\n");

	return ch;
}
