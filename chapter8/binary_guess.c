/* binary_guess.c -- find an integer picked by user*/
#include <stdio.h>


int main(void){
	int min, max, guess;

	min = 1;
	max = 100;
	guess = 50;
	char ch;

	printf("Pick an integer from 1 to 100. I will try to guess");
	printf("it.\nRespond with 'y' if my guess is right, 'h' if it's high ");
	printf("or with 'l' if it's low.\n");
	
	printf("So lets start with %d.Is it right?\n", guess);

	while((ch = getchar()) != 'y'){
		
		if(ch == 'l'){
			min = 0;
			max = guess;
		} else if(ch == 'h'){
			min = guess;
		} else{
			printf("Please use 'y', 'l' or 'h'\n");
			continue;
		}
		
		guess = (min + max) / 2;
		printf("So my next guess: %d\n", guess);
		while(getchar() != '\n')
			continue;
	}
	return 0;
}
