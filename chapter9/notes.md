### Chapter 9 Functions
---

* `stdarg.h` provides a standard way for defining a function with variable number of parameters.

```c
// declaration of printf function that returns int
// first argument must be string
// and there may be further arguments of unspecified nature
int printf(const char *, ...)...
```

* `tail/end recursion` -- recursive call is at the end of the function.

* The address operator: `&`.

* The indirection operator: `*`. -- gives value stored at address `&address`.

```c
int value = 22;
int* pointer = &value;
int val = *ptr;
```

* Prefixes are essential!!!!

```
void swap(int* a, int* b){
	int c = *a;

	*a = *b;
	*b = c;
}
```

* To change values of variables visible outside you must send pointers to the function, not just values.

