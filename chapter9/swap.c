// swap.c -- swaps variables in outside functiion
#include <stdio.h>

void swap(int* a, int* b);

int main(void){
	int x, y;

	x = 9;
	y = 1;
	printf("x: %d, x addresss: %p\n", x, &x);
	printf("y: %d, y addresss: %p\n", y, &y);
	
	swap(&x, &y);
	printf("x: %d, x addresss: %p\n", x, &x);
	printf("y: %d, y addresss: %p\n", y, &y);

	return 0;
}

void swap(int* a, int* b){
	int c = *a;

	*a = *b;
	*b = c;
}
