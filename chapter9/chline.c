// chline.c --prints char in columns i through j
#include <stdio.h>

void chline(char ch, int i, int j);

int main(void){
	
	chline('A', 10, 20);

	return 0;
}

void chline(char ch, int i, int j){
	for(int c = 0; c < j; c++){
		if ( c < i)
			printf(" ");
		else
			printf("%c", ch);
	}
		printf("\n");
}
