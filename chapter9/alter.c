// alter.c -- takes two int numbers and change their values to their summ and
// their difference
#include <stdio.h>

void alter(int * x, int * y);
 
int main(void){
	int a, b;
	a = 10;
	b = 5;

	alter(&a, &b);

	printf("a is: %d\n", a);
	printf("b is: %d\n", b);

	return 0;
}


void alter(int * x, int *  y){
	
	*x += *y;
	*y = *x - 2 * *y;
}
