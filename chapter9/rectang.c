// rectang.c -- prints rectangles from chars
#include <stdio.h>

void rect(char a, int length, int height);

int main(void){

	rect('O', 10, 20);

	return 0;
}

void rect(char a, int length, int height){
	
	int count = length;

	while(height-- > 0){
		while(count-- > 0)
			putchar(a);
		printf("\n");
		count = length;
	}
}
