// min.c -- returns smallest from two numbers
#include <stdio.h>

double min(double a, double b);

int main(void){

	double a, b;
	a = 1;
	b = 2;
	
	printf("%.1lf\n", min(a, b)); 

	return 0;
}

double min(double a, double b){
	if(a > b)
		return a;
	else
		return b;

	return -1;
}
