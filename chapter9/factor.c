// rec_factor.c -- calculates recursion using recursion
#include <stdio.h>

long rec_fact(int n);

int main(void){
	
	int num;

	printf("Enter a value in the range 0-14 (q to quit)");

	while( scanf("%d", &num) == 1){
		if(num < 0)
			printf("No negative numbers\n");
		else if (num > 14)
			printf("%d is too big!", num);
		else {
			printf("loop: %d factorial = %ld\n", num, rec_fact(num));
		}

		printf("Enter a value in the range 0-14 (q to quit): \n");
	}

	printf("Bye.\n");

	return 0;
}

long rec_fact(int n){
	long ans = 1;
	
	while(n-- > 1)
		ans *= n;

	return ans;
}
	
