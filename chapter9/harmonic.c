// harmonic.c -- calculates harmonic mean for two nbumbers
#include <stdio.h>

double harmonic(double a, double b);

int main(void){
	
	double a, b, harm;

	a = 40, b = 60;
	harm = harmonic(a, b);

	printf("%.1lf\n", harm);

	return 0;
}

double harmonic(double a, double b){
	return (2 * a * b) / (a + b);
}
