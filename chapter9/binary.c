// binary.c -- binary representation of int numbers
#include <stdio.h>

void to_binary(unsigned long n);

int main(void){
	unsigned long num;

	printf("enter the number that will be reporesented in binary form: ");

	while(scanf("%lu", &num) == 1){
		printf("Binary equivalent: ");
		to_binary(num);
		putchar('\n');
		printf("Enter an integer or q to quit: ");
	}
	
	printf("Bye!\n");

	return 0;
}

void to_binary(unsigned long n){
	int r;

	r = n % 2;
	if(n >= 2)
		to_binary(n / 2);
	putchar(r == 0 ? '0': '1');

	return;
}
