// add_sort.c -- takes 3 addresses and sort values store in those addresses
// in increasing order
#include <stdio.h>

void add_sort(double *a, double *b, double *c); 

int main(void){
	
	double a, b, c;

	a = 100;
	b = 1;
	c = 10;

	printf("a: %.1lf, b: %.1lf, c: %.1lf\n", a, b, c);
	add_sort(&a, &b, &c);

	printf("After sort: \n");
	printf("a: %.1lf, b: %.1lf, c: %.1lf\n", a, b, c);
	return 0;
}

void add_sort(double *a, double *b, double *c){

	double min, mid, max;
	
	if ( *a > *b && *a > *c)
		max = *a;
	else if(*b > *a && *b > *c)
		max = *b;
	else
		max = *c;
	
	if ( *a < *b && *a < *c)
		min = *a;
	else if(*b < *a && *b < *c)
		min = *b;
	else
		min = *c;  
	
	if ((max == *a || max == *c) && (min == *a || min == *c))
		mid = *b;
	else if ((max == *a || max == *b) && (min == *a || min == *b))
		mid = *c;
	else
		mid = *a;

	*a = min;
	*b = mid;
	*c = max;
}
