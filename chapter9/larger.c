// larger.c -- replaces the contents of  two double variables with the maimum of
// the two variables.

#include <stdio.h>

void larger_of(double *a,  double *b);

int main(void){

	double a, b;

	a = 100;
	b = 10;

	printf("a: %.1lf, b = %.1lf\n", a, b);

	larger_of(&a, &b);
	printf("a: %.1lf, b = %.1lf\n", a, b);

	return 0;
}

void larger_of(double *a, double *b){

	if( *a > *b)
		*b = *a;
	else
		*a = *b;
}
