/* eight_2.c -- creates array of 8 int squares of 2*/
#include <stdio.h>

int main(void){
	
	int array[10];
	int count = 7;
	int twos = 1;

	for(int i = 0; i < 8; i++, twos *= 2)
		array[i] = twos;
	
	do{
		printf("*%d*", array[count]);
	}while(count-- > 0);

	printf("\n");

	return 0;
}
