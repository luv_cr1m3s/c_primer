/* sum_squares.c -- prints sum of squares for integer limit */
#include <stdio.h>

int main(void){

	int low, high, sum;
	
	printf("Enter lower and upper integer limits: ");
	
	while(scanf("%d %d", &low, &high) == 2){
		if(low >= high){
			printf("Done!\n");
			return 0;
		}
		
		sum = 0;
		for(int i = low; i <= high; i++)
			sum += i*i;	

		printf("The sum of the squares from %d to %d is %d\n", low*low, high*high, sum);
		printf("Enter next set of limits: ");
	}


	return 0;
}
