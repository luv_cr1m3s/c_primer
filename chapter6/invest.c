/* invest.c -- calculatest investments of Daphne and Deirde*/
#include <stdio.h>

#define BASE 100
#define DAPPER 10
#define DEIPER 5

int main(void){
	
	int count = 1;
	double dap_sum, dei_sum;

	dap_sum = BASE;
	dei_sum = BASE;
	
	do{
		dap_sum += 10.0;
		dei_sum *= 1.05;
		count++;
	} while(dei_sum < dap_sum);

	printf("Daphne: %.3lf Deirde: %.3lf in %d years\n", dap_sum, dei_sum, count);

	return 0;
}
