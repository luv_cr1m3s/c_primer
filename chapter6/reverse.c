/* reverse.c -- reads line and prints it in reversed order */
#include <stdio.h>

int main(void){

	char array[255];
	int count = 1;
	
	printf("Enter a line: ");

	array[0] = '0';

	while(array[count-1] != '\n' && count <= 256){
		scanf("%c", &array[count]);
		count++;
	}

	while(--count > 0)
		printf("%c", array[count]);
	
	printf("\n");
	return 0;
}



