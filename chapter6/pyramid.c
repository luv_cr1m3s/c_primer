/* pyramid.c -- prints pyramid from letter*/
#include <stdio.h>

#define A 65
#define Z 90

int main(void){
	
	char usr;
	int rows, max_length, spaces;
	
	printf("Enter uppercase letter: ");
	scanf("%c", &usr);

	if((int)usr > Z || (int)usr < A){
		printf("Wrong value!\n");
		return 1;
	}

	rows = (int)usr - A;
	max_length = rows * 2 - 1;
	spaces = 1 + max_length / 2;

	for(int i = 0; i <= rows; i++){
		
		for(int k = 0; k < spaces; k++)
			printf(" ");

		for(int j = 0; j <= i; j++){
			printf("%c", A + j);
		}
		
		for(int n = i - 1; n >= 0; n--)
			printf("%c", A + n);

		spaces--;
		printf("\n");
	}

	return 0;
}


