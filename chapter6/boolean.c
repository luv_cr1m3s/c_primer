/* boolean.c -- example of usage _Bool variable */
#include <stdio.h>
#include <stdbool.h>

int main(void){

	_Bool input = true;
	bool output =  0;

	printf("%d\n%d\n", input, output);

	return 0;
}
