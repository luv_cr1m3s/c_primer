/* user_limit.c -- prints square and cube values for ineteger in diapazone
 * defined by user*/
#include <stdio.h>

int main(void){

	int low, high;

	printf("Plese enter lower and input limits: ");
	scanf("%d", &low);
	scanf("%d", &high);

	if( low > high){
		int tmp = low;
		low = high;
		low = tmp;
	}

	for(int i = low; i <= high; i++){
		printf("*****************\n");
		printf("value: %d, square: %d, cube: %d\n", i, i*i, i*i*i);
	}

	printf("****************\n");

	return 0;
}
