/* two_float.c -- reads two float numbers from, user and prints the value of their
* difference divided by their product*/
#include <stdio.h>

double some_math(double a, double b);

int main(void){

	double one, two;
	printf("Enter nonnumbers to exit\n");
	printf("Enter two float numbers to start: ");

	while(scanf("%lf %lf", &one, &two) == 2)
		printf("|--->>%lf\nEnter next two numbers to continue: ", some_math(one, two));


	return 0;
}

double some_math(double a, double b){
	return (a - b)/(a*b);
}
