/* azbtria.c -- prints triangle from alphabet letters */
#include <stdio.h>

#define A 65

int main(void){
	
	int count = 0;

	for(int i = 0; i < 6; i++){
		for(int j = 0; j <= i; j++, count++)
			printf("%c", A + count);

		printf("\n");
	}

	return 0;
}
