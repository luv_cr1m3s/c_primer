/* million.c -- find how many years you need to empty acc with 1m$ */
#include <stdio.h>

int main(void){

	double start, percent, wind;
	int count = 1;


	start = 1000000.0;
	percent = 1.08;
	wind = 100000.0;

	while(start > wind){
		start *= percent;
		start -= wind;
		count++;
	}

	printf("It took: %d, and now on the acc left: %.3lf.\n", count, start);

	return 0;
}
