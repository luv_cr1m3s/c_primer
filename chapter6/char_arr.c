/* char_arr.c -- reads user input into char array and prints it in backward*/
#include <stdio.h>

int main(void){

	char arr[20];
	int count = 0;

	printf("Enter a word:");

	do{
		scanf("%c", &arr[count]);
		count++;
	}while(arr[count-1] != ' ');
	
	count -=  1;
	while(count-- >= 0)
		printf("%c", arr[count]);

	printf("\n");

	return 0;
}
