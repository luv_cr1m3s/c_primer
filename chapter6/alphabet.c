/* alphabet.c -- stores 26 chars and prints them */
#include <stdio.h>

#define ALPHABET 26
#define START 97

int main(void){

	char alpha[ALPHABET];

	for(int j = 0; j < ALPHABET; j++)
		alpha[j] = START + j;
	
	for(int i = 0; i < ALPHABET; i++)
		printf("%c\n", alpha[i]);

	return 0;
}
