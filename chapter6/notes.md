### Chapter 6 C Control Statements
---

* Function `fabs()` from `math.h` could be used for comparing floating point values.

```c
while( fabs(a - b) > 0.001)...
```

* `stdbool.h` provides alias `bool` for type `_Bool` and defines `true` and `false` as symbolic constants for values 1 and 0.

```c
ex != wye == zee 
//eq to 
(ex ~= wye) == zee
```


