/* alptria.c -- printfs triangle from chars */
#include <stdio.h>

#define START 70

int main(void){

	for(int i = 0; i < 6; i++){
		for(int j = 0; j <= i; j++)
			printf("%c", START - j);

		printf("\n");
	}

	return 0;
}
