/* nested.c -- exeercise 6 */
#include <stdio.h>

#define ROWS 6
#define LINES 8

int main(void){
	
	for(int i = 0; i < ROWS; i++){
		for(int j = 0; j < LINES; j++)
			printf("$");

		printf("\n");
	}

	return 0;
}
