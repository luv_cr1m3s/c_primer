/* double_array.c -- creates two arrays based on user input and prints them */
#include <stdio.h>

int main(void){

	double first[8], second[8];

	for(int i = 0; i < 8; i++){
		printf("Enter %d element of array: ", i + 1);
		scanf("%lf", &first[i]);
	}

	second[0] = first[0];

	for(int j = 1; j < 8; j++)
		second[j] = second[j-1] + first[j];

	for(int k = 0; k < 8; k++)
		printf("<-%.2lf->", first[k]);

	printf("\n");

	for(int y = 0; y < 8; y++)
		printf("-<%.2lf>-", second[y]);

	printf("\n");

	return 0;
}
