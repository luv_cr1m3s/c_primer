/* dunbar.c -- finds how long it take to reach Dunbar's number for strnge prof*/
#include <stdio.h>

#define DUNBAR 150

int main(void){

	int count, friends;

	count = 1;
	friends = 5;

	while(friends < DUNBAR){
		friends -= count;
		friends *= 2;
		count++;
	}

	printf("Fort our prof it took %d weeks to reach Dunbar's" 
			"number with %d number of friends.\n", count, friends);

	return 0;
}
