/* iterations.c -- ask user aboiut number of iterations and prints result of two
 * sequences*/
#include <stdio.h>

int main(void){

	int limit;
	double first, second;

	printf("Enter number of iterations: ");
	scanf("%d", &limit);
	
	if(limit <= 0){
		printf("Done!\n");
		return 0;
	}

	first = 0;
	second = 0;

	for(int i = 1; i <= limit; i++){
		first += 1.0 / i;
		second += (i%2 == 0)? -1.0/i : 1.0/i;
	}
	
	printf("First seq after %d iterations: %lf\n", limit, first);
	printf("Second seq after %d iterations: %lf\n", limit, second);

	return 0;
}
