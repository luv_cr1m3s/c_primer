/* eight_int.c -- reads 8 int into array*/
#include <stdio.h>

int main(void){

	int arr[8];

	printf("Print eight int numbers with spaces: ");
	for(int i = 0; i < 8; i++)
		scanf("%d", &arr[i]);

	for(int j = 7; j >= 0; j--)
		printf("%d", arr[j]);

	printf("\n");
	return 0;
}
