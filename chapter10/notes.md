## Chapter 10 Arrays and pointers
---

* Recommended practice if predefined arrays size with `#define` directive.

```c
#define MONTH 12
...
int days[MONTH] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
...
```

* You can create read-only array with `const` prefix:

```c
const int days[MONTH] = ...
```

* If you don't initialize an array at all, its elements get garbage values.
* If you partially initialize an array non initialized values get `0`'s values.

* To get array size use: 

```c
int arrya_length = sizeof array / sizeof array[0];
```

* In `C99` was added `designated initializers`:

```c
// initialization of only 6th element before C99
int arr[6] = {0,0,0,0,0,1};
// C99 designated initialization, ather elements automatically sert to '0'
int arr[6] = {[5] = 212};
int days[MONTH] = {31, 28, [4] = 31, 30, 31, [1] = 29};
// 31 29 0 0 31 30 31 0 0 0 0 0 
```

* Size of the array on initialization mast be greater than 0 and integer.
* `C99` allows creating `variable-length array`:

```c
float array[n];
```

* Two ways of initializing of two-dimensional array:

```c
int array[2][3] = {{5, 6}, {7, 8, 9}}; // {5, 6, 0}, {7, 8, 9} 

int array[2][3] = {5, 6, 7, 8}; // {5, 6, 7}, {8, 0, 0}
```

* Array name is the address of the first element of array:

```c
array == &array[0];
```
* In prototypes you can omit parameters names:

```c
// that's all the same
int sum(int *ar, int n);
int sum(int *, int);
int sum(int ar[], int n);
int sum(int [], int n);
```

* But you can't omit parameters names in the function definition, so previous examples are now valid for them.

* You can iterate array with `*pointer++ or *++pinter`, but `(*pointer)++` will return value stored at pointer address increased by 1.

* Never assign digits to the pointer:

```c
int *pointer;
*pointer = 5; // it could harm memory
```

* Protection for array from being modified by function:

```c
int sum(const int ar[], int n)...
// it tells to function treat array as though it were constant
```

* Only the address of non-constant data can be assigned to regular pointers:

```c
double rates[2] = {1.2, 1.3};
const double locked[1] = {4};
double * pnc = rates; // OK
pnc = locked; 				// not valid
pnc = &rates[0]; 			// OK
```

* You can use `const` twice to create a pointer that can neither change where it's pointing nor change the value to which it points:

```c
const  double * const locked_pointer = rates;
```

* Compound literals were added in `C99`:

```c
// compound literal that creates a nameless array containign two int values
(int [2]){10, 20}; 
```


