// pnt_add.c -- pointer addition
#include <stdio.h>

#define SIZE 4

int main(void){
	
	short data [SIZE];
	short *pti;
	short i;
	double arr[SIZE];
	double *ptf;

	pti = data;
	ptf = arr;
	
	printf("%23s %15s\n", "short", "double");
	for (i = 0; i < SIZE; i++){
		printf("pointers + %d: %10p %10p\n",
				i, pti + i, ptf + i);
	}

	return 0;
}
