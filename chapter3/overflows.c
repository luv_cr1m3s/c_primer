/* overflows.c -- program to find out how compiler handles over and underflow */
#include <stdio.h>
#include <limits.h>
#include <float.h>

int main(void){
	
	float flo = FLT_MAX;
	double doub = DBL_MAX;
	long double ldoub = LDBL_MAX;

	printf("Here is overflowed value for integer %d: %d.\n", INT_MAX, INT_MAX + 1);
	// FLT_EPSILON -- difference between 1.00 and the least float value 
	// greater than 1.00
	printf("Here is overflowed value for float: %f.\n", flo * (1 + FLT_EPSILON));
	printf("Here is overflowed value for double: %f.\n", doub * (1 + FLT_EPSILON));
	printf("Here is overflowed value for long double: %Lf.\n", ldoub * (1 + FLT_EPSILON));
	printf("Here is underflowed value for float %e: %e.\n",FLT_MIN, FLT_MIN / 10.0);
	printf("Here is underflowed value for double %e: %e.\n", DBL_MIN,  DBL_MIN / 10.0);
	printf("Here is underflowed value for long double %Le: %Le.\n",LDBL_MIN, LDBL_MIN / 10.0);
	return 0;
}
