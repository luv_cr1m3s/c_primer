/* alert.c -- program that produce sound and print some text */
#include <stdio.h>

int main(void){

	printf("\aStartled by the sudden sound, Sally shouted,\n\"By the GReat Pumpkin, what was that!\"\n");

	return 0;
}
