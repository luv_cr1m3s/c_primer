/* centi_inch.c -- converts your height in cm into inches */
#include <stdio.h>

int main(void){

	float inch = 2.54;
	float height;
	printf("Enter you height in centimeters: ");
	scanf("%f", &height);
	printf("You height in inches is: %.3f.\n", height / inch);

	return 0;
}
