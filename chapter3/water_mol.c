/* water_mol.c -- finds number of water molecules in human body */
#include <stdio.h>

int main(void){

	long double water_mol_weight = 3e-23;
	long double user_weight;
	printf("Please enter your weight: ");
	scanf("%Lf", &user_weight);
	printf("\nThere is %.3Le water molecules in your body.\n", user_weight / water_mol_weight);

	return 0;
}

