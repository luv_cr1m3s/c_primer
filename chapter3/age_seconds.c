/* age_seconds converts number of years into seconds */
#include <stdio.h>

int main(void){

	long double age;
	long double sec_year = 3.156e+07;
	printf("Enter your age in years: ");
	scanf("%Lf", &age);

	printf("You lived for: %Le seconds.\n", age * sec_year);

	return 0;
}

