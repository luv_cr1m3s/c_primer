/* america.c -- converts cups in other strange measurement systems */
#include <stdio.h>

int main(void){

	float cups;
	printf("Enter volume in cups: ");
	scanf("%f", &cups);

	printf("%.3f cups is:\n", cups);
	printf("pints: %.3f,\nounces: %.3f,\ntablespoons: %.3f,\nteaspoons: %.3f.\n", 
			cups / 2, cups * 8, cups * 16, cups * 48);

	return 0;
}
