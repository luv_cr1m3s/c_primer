/* ascii.c -- program that ask user toi input int value and converts it into
 * ASCII symbol. */
#include <stdio.h>

int main(void){
	
	int input;
	
	printf("Please enter the number (should be >=0 and <128): ");
	scanf("%d", &input);
	printf("\nYou entered %d that was converted into %c.\n", input, input);


	return 0;
}
