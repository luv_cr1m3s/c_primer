/* floatformat.c -- program that prints float variables in difeferent formats */
#include <stdio.h>

int main(void){

	long double input;
	printf("Enter a floating-point value: ");
	scanf("%Lf", &input);
	printf("fixed-point notation: %Lf\n", input);
	printf("exponentioal notation: %Le\n", input);
	printf("p notation: %La\n", input);

	return 0;
}
