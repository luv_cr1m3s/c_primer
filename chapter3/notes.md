### Data in C

## C Data Keywords:
---

* Original K&R:

```c
int     long    short   double
char    float   unsigned
```

* C90:

```c
signed  void
```

* C99:

```c
_Bool   _Complex    _Imaginary
```

* `Imaginary number` -- number with base 'i'. 
* `Complex number` -- number that consist from real number and imaginary part.

## Basic C Data Types
---

* To print integer in octal or hexadecimal form use `%o` or `%x`, to show prefixes `%#o`, `%#x` or `%#X`.

* To mark int value as `long` or `long long` use `l` or `L` at the end of the number:
7LL, 8l, 0x10L... In the same way you can mark value as unsigned.

* To print unsigned use `%u`, for long `ld`, also you can specify base by adding standard formatters `%lld`, `%lu`, `%lx`, `%l0`...

* It's possible to assign `char grade = 'FATE';`, but it will store only the last 8 bits ('E').

* You can assign char through numbers: `char bepp = 7;`.

* To interpret numbers as octal and use for chars use `char beep = '\7'` notation, leading '0' can be omitted.

* To interpret numbers as hexadecimal use `char ctrp = '\x10'` or `'\X010`.

* In `C99` and `C11` created library to support portable types. `inttypes.h` include `stdint.h` and could be used to define:

> fixed size i.e. `int32_t`...
> minimum width i.e. `int_least8_t`...
> fastest minimum width i.e. `int_fast8_t`...
> biggest possible type i.e. `intmax_t`, `uintmax_t`...
> also included macros to display the portable types.

* Since `C99`, C has a new format for float variables. It uses hexadecimal prefix ('0x' or '0X'), a 'p' or 'P' instead of 'e' or 'E', and an exponent that is a power of 2:

```c
float num = 0xa.1fp10 // (a->10 + .1f -> 1/16 + 15/256) * (p10 -> 2^10) = 10364.0
```

* `C99` provides a `%zd` specifier for sizes.

* `float` is guaranteed to represent only the first six digits accurately.

