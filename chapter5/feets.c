/* feets.c -- converts centimeters into feets and inches */
#include <stdio.h>

#define FEET 12.0 // inches in feet
#define INCH 2.54 // cm in inch
#define INCH_FEET 12 // inches in feet
int main(void){

	float centimeters, inches;
	int feet;
	
	printf("Enter height in cm: ");
	scanf("%f", &centimeters);
	
	while(centimeters > 0){
		inches = centimeters / INCH;
		feet = (int)inches / INCH_FEET;

		printf("%.1f cm = %.1d feet, %.1f inches.\n",
				centimeters, feet, inches - INCH_FEET * feet);
		
		printf("Enter height in cm: ");
    scanf("%f", &centimeters);
	}

	printf("That's it you entered value < 0\n");
	return 0;
}
