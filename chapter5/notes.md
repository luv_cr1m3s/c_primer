### Chapter 5 Operators, Expressions, and Statements
---

`a%b == a  - (a/b)*b`

* You can use increment operator in `while` loop: `while(++value < 0)`.

```c
int a = 1, b = 1;
int a_post, b_pre;

a_post = a++;
b_pre = ++b;
// in result a = 2, a_post = 1, b = 2, b_pre = 2
```
* Don't use increment or decrement operators:
> on a variable that is part of more than one argument of a function.  
> on a variable that appears more than once in an expression.

