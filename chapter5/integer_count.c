/* integer_count.c -- asks for number and prints numn..num+10 */
#include <stdio.h>

int main(void){

	int num, count;

	printf("Enter the number: ");
	scanf("%d", &num);
	count = num + 10;
	num--;

	while(num++ < count)
		printf("%d\n", num);
		
	return 0;
}
