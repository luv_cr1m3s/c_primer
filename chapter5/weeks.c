/* weeks.c -- converts days into weeks and days */
#include <stdio.h>

#define WEEK 7

int main(void){
	
	int days;

	printf("Enter number of days: ");
	scanf("%d", &days);

	while( days > 0){
		printf("Weeks: %d days: %d\n", days / WEEK, days % WEEK);
		printf("Enter number of days: ");
		scanf("%d", &days);
	}
	printf("That's it you entered zero or negative number\n");
	return 0;
}
