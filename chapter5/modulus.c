/* modulus.c -- calculates modulus result for user defiuned base and numbers*/
#include <stdio.h>

int main(void){

	int base, number;

	printf("This program computes moduli.\n");
	printf("Enter an integer to serve as the second operand: ");
	scanf("%d", &base);
	printf("Now enter the first operand: ");
	scanf("%d", &number);

	while(number > 0){
		printf("%d %% %d is %d\n", number, base, number % base);
		printf("Enter next number for first operand(<= 0 to quit): ");
		scanf("%d", &number);
	}

	printf("Tht's it, see you!\n");

	return 0;
}
