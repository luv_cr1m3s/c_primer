/* cubed.c -- asks anbout number and prints cube of that value */
#include <stdio.h>

double qube(double a);

int main(void){
	
	double number;

	printf("Enter value that should be qubed: ");
	scanf("%lf", &number);

	printf("Qube of %.3lf is: %.3lf\n", number, qube(number));

	return 0;
}

double qube(double a){
	return a * a * a;
}
