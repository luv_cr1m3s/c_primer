/* temperatures.c -- program that converts fahrenheit temperature to celsius and
 * kelvin*/
#include <stdio.h>

void temperatures(double fahr);

int main(void){

	double fahr;
	printf("Enter temperatuer in fahrs: ");

	while(scanf("%lf", &fahr)){
		temperatures(fahr);
		printf("Enter next value: ");
	}

	printf("See you!\n");

	return 0;
}

void temperatures(double fahr){
	double celsius, kelvin;

	celsius = 5.0 / 9.0 * (fahr - 32.0);
	kelvin = celsius + 273;

	printf("%.2lf degrees in Fahrenheit equals to:\n", fahr);
	printf("Celsius: %.2lf\n", celsius);
	printf("Kelvin: %.2f\n\n", kelvin);
}
