/* square.c -- calculates squares */
#include <stdio.h>

int main(void){

	int sum, count, i;

	printf("Please enter how far should we go: ");
	scanf("%d", &count);
	
	i = 0;
	sum = 0;

	while( ++i <= count)
		sum += i * i;

	printf("Result after %d iterations: %d\n", count, sum);

	return 0;
}
