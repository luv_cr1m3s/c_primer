/* sieve.c -- asks user to input number and prints all prime numbers less or
 * equal toit */
#include <stdio.h>
#define N 100000

int main(void){

	unsigned int input, count;
	int array[N] = {0};
	count = 2;

	printf("Enter your number: ");	
	scanf("%ud", &input);

	while(count * count  < input){
		for(int i = count * count; i < input; i += count){
			array[i] = 1;
	}

		count++;
	}

	for(int i = 2; i < input; i++){
		if(array[i] == 0)
			printf("%6d ", i);
	}

	printf("\n");
	return 0;
}


