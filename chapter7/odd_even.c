/* odd_even.c -- reads user input and counts number of odd/even numbers*/
#include <stdio.h>
#include <ctype.h>

int main(void){

	int tmp;
	unsigned int odd, even;
	float odd_mid, even_mid;
	char ch;

	odd = even = 0;
	odd_mid = even_mid = 0;

	printf("Enter your integers ('0' to exit): ");
	while((ch = getchar()) != '0'){
		if(isdigit(ch)){
			tmp = ch - '0';
			if(tmp % 2 != 0){
				odd++;
				odd_mid += (float)tmp;
			}
			else{
				even++;
				even_mid += (float)tmp;
			}
		}
	}
	
	printf("Number of even numbers: %d, avrg value is: %.3f\n",
			even, even_mid / (float)even);
	printf("Number of odd numbers: %d, avrg value is: %.3f\n",
			odd, odd_mid / (float)odd);

	return 0;
}
