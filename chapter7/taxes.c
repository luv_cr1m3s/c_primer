/* taxes.c -- calculates salary and taxes for worked hours */
#include <stdio.h>

#define BASE 10.0				//base payment 10 dol/hour
#define BASEHOURS 40.0	//base working week hours
#define OVERTIME 1.5		//coeficient for overtime, hours > 40
#define FTAX 0.15		  	//tax percent of the first 300$
#define BFTAX 300.0			//300$ rate
#define STAX 0.2				//tax rate of the next 150$
#define BSTAX 150.0			//150$ rate
#define TTAX 0.25				//tax rate for the rest

int main(void){

	float hours, gross_pay, taxes, net_pay;
	float temp;

	hours = gross_pay = taxes = net_pay = 0;

	printf("Please enter hours worked iin the week: ");
	scanf("%f", &hours);
	
	if(hours > BASEHOURS)
		gross_pay = BASEHOURS * BASE + (hours - BASEHOURS) * BASE * OVERTIME;
	else
		gross_pay = hours * BASE;
	
	temp = gross_pay;
	if(gross_pay > (BSTAX + BFTAX)){
		temp -= BSTAX + BFTAX;
		taxes = temp * TTAX + BFTAX * FTAX + BSTAX * STAX;
	} else if(gross_pay <= BFTAX){
		taxes = gross_pay * FTAX;
	} else if(gross_pay <= (BSTAX + BFTAX)){
  	temp -= BSTAX;
  	taxes = temp * STAX + BFTAX * FTAX;
	} else {
		printf("Number of hours can't be negative!\n");
		return -1;
	}

	net_pay = gross_pay - taxes;

	printf("Here is result for %f worked hours:\n", hours);
	printf("Gross salary: %.3f\n", gross_pay);
	printf("Taxes: %.3f\n", taxes);
	printf("Net salary: %.3f\n", net_pay);

	return 0;
}
