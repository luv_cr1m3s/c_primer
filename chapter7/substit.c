/* substit.c -- reads user input and make some substitution */
#include <stdio.h>

int main(void){

	char ch;
	unsigned int subs = 0;

	printf("Start your input below:\n");
	printf("--> ");

	while((ch = getchar()) != '#'){
		if(ch == '.'){
			printf("!");
			subs++;
		} else if(ch == '!'){
			printf("!!");
			subs++;
		} else {
			printf("%c", ch);
		}
	}
	
	printf("\nNUmber of substitutions: %hd\n", subs);

	return 0;
}
