/* fake_sieve.c -- asks user to input number and prints all prime numbers less or
 * equal toit */
#include <stdio.h>

int main(void){

	unsigned int input, count, flag, i;

	count = 2;

	printf("Enter your number: ");	
	scanf("%ud", &input);

	while(count < input){
		flag = 0;

		for(i = 2; i < count; i++){
			if(count % i == 0)
				flag++;
		}

		if( flag == 0)
			printf("%d ", i);
		count++;
	}
	printf("\n");
	return 0;
}


