/* cypher2.c -- alters input, preserving non-letters */
#include <stdio.h>
#include <ctype.h>

#define SPACE ' '

int main(void){

	char ch;

	while((ch = getchar()) != EOF){
		if(isalpha(ch))
			putchar(ch + 1);
		else
			putchar(ch);
	}

	return 0;
}

