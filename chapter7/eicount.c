/* eicount.c -- reads user input and counts number of 'ei'*/
#include <stdio.h>

int main(void){

	char ch;
	unsigned int count, mark;

	count = mark = 0;

	printf("Enter your input below:\n");
	printf("-->");

	while((ch = getchar()) != '#'){
		if(ch == 'e'){
			mark = 1;
		} else if( ch == 'i' && mark == 1){
			count++;
			mark = 0;
		} else{
			mark = 0;
		}
	}

	printf("Number of 'ei' is: %hd\n", count);

	return 0;
}
