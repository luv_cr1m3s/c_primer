/* substit.c -- reads user input and make some substitution */
#include <stdio.h>

int main(void){

	char ch;
	unsigned int subs = 0;

	printf("Start your input below:\n");
	printf("--> ");

	while((ch = getchar()) != '#'){	
		switch(ch){
			case '.':
				printf("!");
				subs++;
				break;
			case '!':
				printf("!!");
				subs++;
				break;
			default:
				printf("%c", ch);
				break;
		}
	}
	
	printf("\nNUmber of substitutions: %hd\n", subs);

	return 0;
}
