/* taxes1988.c -- calculates taxes for chosen category baesd on 1988n Federal
 * Tax Shedule */
#include <stdio.h>

#define BASE 0.15
#define OVERBASE 0.28
#define SINGLE 17850.0
#define HOUSEHEAD 23900.0
#define MJOINT 29750.0
#define MSEPARATE 14875.0

float tax_calc(float salary, float base);

int main(void){

	float salary, taxes;
	int flag;
	salary = taxes = 0;

	while(1){
		printf("***************************************************\n");
		printf("Choose tax category:\n");
		printf("1) Single\n2) Head of Household\n3) Married, Joint\n"
				"4) Married, Separate\n5) Quit\n");
		printf("***************************************************\n");
		printf("Enter 1, 2, 3, 4 or 5 to quit: ");
		scanf("%d", &flag);

		printf("Now enter your salary: ");
		scanf("%f", &salary);
		
		if(salary <= 0){
			printf("Salary can't be eq or less than 0! Try again.\n");
			continue;
		}

		switch(flag){
			case 1:
				taxes = tax_calc(salary, SINGLE);
				break;
			case 2:
				taxes = tax_calc(salary, HOUSEHEAD);
				break;
			case 3:
				taxes = tax_calc(salary, MJOINT);
				break;
			case 4:
				taxes = tax_calc(salary, MSEPARATE);
				break;
			case 5:
				printf("See you!\n");
				return 0;
				break;
			default:
				printf("Wrong argument %d, try again\n", flag);
				continue;
				break;
		}
		printf("__________________________________________\n");
		printf("| Here is result for based on your salary:\n");
		printf("| Gross salary: %.3f\n", salary);
		printf("| Taxes: %.3f\n", taxes);
		printf("| Net salary: %.3f\n", salary - taxes);
		printf("__________________________________________\n");
	}
	return 0;
}

float tax_calc(float salary, float base){
	float taxes = 0;

	if( salary > base)
		taxes = base * BASE + (salary - base) * OVERBASE;
	else
		taxes = salary * BASE;

	return taxes;
}
