/* prime.c -- checks user input for prime numbers */
#include <stdio.h>
#include <stdbool.h>

int main(void){

	unsigned long num, div;
	bool isPrime;

	printf("Enter an integer for analyssis; ");
	printf("Enter q to quit: ");

	while(scanf("%lu", &num) == 1){
		
		for(div = 2, isPrime = true; (div*div) <= num; div++){
			
			if(num % div == 0){
				if(div * div != num)
					printf("%lu is divisible by %lu and %lu\n", num, div, num / div);
				else
					printf("%lu is divisible by %lu\n", num, div);
				isPrime = false;
			}
		}
		if(isPrime)
			printf("%lu is prime!!!\n", num);

		printf("Enter next number of 'q' to exit: ");
	}

	printf("Bye.\n");

	return 0;
}
