### Chapter 7 C Control Statements
---

* For char processing it's better to use `getchar()` and `putchar()` from 'stdio.h' and functions for type check from `ctype.h`.

* `C99` requires that a compiler support a minimum of 127 levels of nesting.

* `else` goes with the most recent if unless braces indicate otherwise.

* In `iso646.h` included `or`, `and` and `not` operators to replace `||`, `&&` and `
!`.

```c
( range >= 90 && range <= 100) // ok
( 90 <= range <= 100) // semantic error. never use it!
//( 90 <= range) <= 100 == 1 <= 100 -- true all the time
```

* `switch` can't be used for 'floating point' types and for the range (i.e. you will need to declare behaviour for each case).


