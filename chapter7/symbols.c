/* counts number of spaces, newlines, chars and other symbols in user input */
#include <stdio.h>
#include <ctype.h>

int main(void){

	unsigned short space, newline, other;
	char ch;

	space = 0;
	newline = 0;
	other = 0;
	
	printf("Enter your text below, to finish enter #\n");

	while((ch = getchar()) != '#'){
		switch(ch){
			case ' ':
				space++;
				break;
			case '\n':
				newline++;
				break;
			default:
				other++;
				break;
		}
	}

	printf("Spaces: %hd, newlines: %hd, other symbols: %hd\n", space, newline, other);

	return 0;
}
