/* 8chars.c -- reads user input and prints chars and their int representation*/
#include <stdio.h>

int main(void){

	char ch;
	int count = 0;
	
	printf("Start typing here (to finish type '#'): ");

	while((ch = getchar()) != '#'){
		if(ch == '\n')
			printf("\'\\n\'->%d ", (int)ch);
		else
			printf("\'%c\'->%d ", ch, (int)ch);
		
		if(++count % 8 == 0)
			printf("\n");
	}
	
	printf("\nDone!\n");

	return 0;
}
