/* taxes_p2.c -- calculates salary and taxes for worked hours */
#include <stdio.h>

#define BASEHOURS 40.0	//base working week hours
#define OVERTIME 1.5		//coeficient for overtime, hours > 40
#define FTAX 0.15		  	//tax percent of the first 300$
#define BFTAX 300.0			//300$ rate
#define STAX 0.2				//tax rate of the next 150$
#define BSTAX 150.0			//150$ rate
#define TTAX 0.25				//tax rate for the rest

int main(void){

	float hours, gross_pay, taxes, net_pay, base_pay;
	float temp;
	int flag;
	hours = gross_pay = taxes = net_pay = 0;
	
	while(1){
		
		printf("*****************************************************************\n");
		printf("Enter the number corresponding to the desired pay rate or action:\n");
		printf("1) $8.75/hr                         2) $9.33/hr\n"
					 "3) $10.00/hr                        4) $11.20/hr\n"
					 "5) quit\n");
		printf("*****************************************************************\n");
		printf("--> ");
		
		scanf("%d", &flag);
		
		switch(flag){
			case 1:
				base_pay = 8.75;
				break;
			case 2:
				base_pay = 9.33;
				break;
			case 3:
				base_pay = 10.00;
				break;
			case 4:
				base_pay = 11.20;
				break;
			case 5:
				printf("That's it\n");
				return 1;
				break;
			default:
				printf("Try again!\n");
				continue;
				break;
		}
		
		printf("Please enter hours worked in the week: ");
		scanf("%f", &hours);
	
		if(hours > BASEHOURS)
			gross_pay = BASEHOURS * base_pay + (hours - BASEHOURS) * base_pay * OVERTIME;
		else
			gross_pay = hours * base_pay;
	
		temp = gross_pay;
		if(gross_pay > (BSTAX + BFTAX)){
			temp -= BSTAX + BFTAX;
			taxes = temp * TTAX + BFTAX * FTAX + BSTAX * STAX;
		} else if(gross_pay <= BFTAX){
			taxes = gross_pay * FTAX;
		} else if(gross_pay <= (BSTAX + BFTAX)){
  		temp -= BSTAX;
  		taxes = temp * STAX + BFTAX * FTAX;
		} else {
			printf("Number of hours can't be negative!\n");
			return -1;
		}

		net_pay = gross_pay - taxes;

		printf("Here is result for %f worked hours:\n", hours);
		printf("Gross salary: %.3f\n", gross_pay);
		printf("Taxes: %.3f\n", taxes);
		printf("Net salary: %.3f\n", net_pay);
	}
	
	return 0;
}
